#Clean up previous distributions
if (Test-Path dist) {
    Remove-Item dist -Recurse -Force
}
#Clean up previous build
if (Test-Path build) {
    Remove-Item build -Recurse -Force
}

#Variable pointing to NGC
$NGC = "node_modules/.bin/ngc.cmd"
$ROLLUP="node_modules/.bin/rollup.cmd"

#Run Angular Compiler
& "$NGC" -p src/tsconfig.json

# Rollup angular-library-seed-jason.js
& "$ROLLUP" build/angular-library-seed-jason.js -o dist/angular-library-seed-jason.js

# Copy non-js files from build
Copy-Item -Exclude *.js -Recurse -Path build/* -Destination dist

#Copy package.json from src/package.json to dist/package.json
Copy-Item -Path src/package.json -Destination dist/package.json